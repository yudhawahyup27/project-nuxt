module.exports = {
  theme: {
    Container: {
      center: true,
      padding: '16px'
    },
    extend: {
      colors: {
        primary: {
          50: '#323232',
          100: '#282828',
          200: '#1e1e1e',
          300: '#141414',
          400: '#0a0a0a',
          500: '#000000',
          600: '#000000',
          700: '#000000',
          800: '#000000',
          900: '#000000'
        },
        screens: {
          '2xl': '1320px'
        }
      }
    }
  }
}
